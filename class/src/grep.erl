-module(grep).
-behaviour(application).

-export([start/2, stop/1, match_emails/1, email_match_from_dir/1, email_match_from_dir/2]).

-ifdef(TEST).
-include_lib("eunit/include/eunit.hrl").
-endif.

start(_StartType, _StartArgs) ->
    ok.

stop(_State) ->
    ok.

match_emails(Text) ->
    case re:run(Text, "[\\w\\.-]+@[\\w\\.-]+", [{capture, all, list}, global]) of
        {match, What} -> lists:usort(lists:append(What));
        nomatch -> []
    end.

read_file(Name) ->
    case file:read_file(Name) of
        {ok, Result} -> binary_to_list(Result);
        {error, Reason} -> throw({error, Name, Reason})
    end.

%list_files(Path) ->
%    filelib:fold_files(Path, "", true, fun(File_name, Acc)-> [File_name|Acc] end, []).


email_match_from_dir(Path, Pattern) ->
    lists:usort(lists:append(filelib:fold_files(Path,
                       Pattern,
                       true,
                       fun(File_name, Acc)->
                           [match_emails(read_file(File_name)) | Acc] end,
                       []))).

email_match_from_dir(Path) ->
    email_match_from_dir(Path, "").

get_worker_nodes() ->
    net_kernel:start([test,shortnames]),
    erlang:set_cookie(node(),dojo),
    net_adm:ping(bootstrap@dojo),
    timer:sleep(1000),
    Data = nodes(),

    lists:filter(fun(Atom) ->
                        string:substr(atom_to_list(Atom), 1, 6) == "worker" end,
                     Data).

-ifdef(TEST).

-define(EMAIL, "abc@def.com").
-define(EMAIL1, "abc1@def.com").
-define(EMAIL2, "newabc@def.com").
-define(TESTFILE1, "../data/test.txt").
-define(TESTFILE2, "../data/test2.txt").
-define(DIR, "../data").
-define(DIR2, "/usr/share/doc/").

match_email_test() ->
    Text = "before " ++ ?EMAIL ++ " after",
    ?assertEqual([?EMAIL], match_emails(Text)).

match_emails_test() ->
    Text = "before " ++ ?EMAIL ++ " after\n new line " ++ ?EMAIL1,
    ?assert(lists:usort([?EMAIL, ?EMAIL1]) == match_emails(Text)).

match_unique_email_test() ->
    Text = "before " ++ ?EMAIL ++ " after\n new line " ++ ?EMAIL,
    ?assert([?EMAIL] == match_emails(Text)).

file_read_test() ->
    Text = read_file(?TESTFILE1),
    ?assert( "before abc@def.com after\nabc1@def.com\n" == Text).

email_match_from_file_test() ->
    File = read_file(?TESTFILE1),
    ?assert(lists:usort([?EMAIL, ?EMAIL1]) == match_emails(File)).

email_match_from_dir_test() ->
    ?assert(lists:usort([?EMAIL, ?EMAIL1, ?EMAIL2]) == email_match_from_dir(?DIR)).


tralala_test() ->
    %Nodes = [worker@dojo,worker2@dojo,bootstrap@dojo,worker@hal9011,worker2@hal9011,bootstrap@hal9011],
    ?assert(lists:usort([worker@dojo,worker2@dojo,worker@hal9011,worker2@hal9011]) == lists:sort(get_worker_nodes())).

-endif.
