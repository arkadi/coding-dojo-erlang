-module(grep).

-behaviour(application).

%% Module API
-export([start/0, stop/0, grep/1, local_grep/1, maintain_workers/0, start_worker/0]).

%% OTP application behaviour callbacks
-export([start/2, stop/1]).

-ifdef(TEST).
-include_lib("eunit/include/eunit.hrl").
-endif.

%% OTP application behaviour callbacks

start(_StartType, _StartArgs) ->
    ok = ensure_contact(),
    Pid = spawn(fun maintain_workers/0),
    {ok, Pid}.

stop(_State) ->
    ok.

% clustered search
start() -> application:start(?MODULE).
stop() -> application:stop(?MODULE).

grep(Dir) ->
    Files = scan_dir(Dir),
    NFiles = length(Files),
    Nodes = worker_nodes(),

    {Workers, _} = lists:split(NFiles, lists:append(lists:duplicate(round(NFiles/length(Nodes))+1, Nodes))),

    Tasks = lists:zip(Files, Workers),
    lists:foreach(
        fun({FileName, Worker}) ->
            Text = read_file(FileName),
            %io:fwrite("sending to ~p~n", [Worker]),
            {grep_worker, Worker} ! {self(), Text}
        end, Tasks),
    lists:usort(lists:append(recv(NFiles))).

recv(N) -> c:flush(), recv(N, []).
recv(0, Acc) -> Acc;
recv(N, Acc) ->
    receive
        Emails -> recv(N-1, [Emails | Acc])
    after 1000 -> Acc
    end.

worker_nodes() ->
    lists:filter(
        fun (Node) ->
            string:str(atom_to_list(Node), "worker") == 1
        end, nodes()).

maintain_workers() -> maintain_workers([]).
maintain_workers(WorkerNodes) ->
    AllNodes = worker_nodes(),
    NewNodes = lists:subtract(AllNodes, WorkerNodes),
    InitializedNodes = init_workers(NewNodes),
    timer:sleep(1000),
    maintain_workers(WorkerNodes ++ InitializedNodes).

init_workers([]) -> [];
init_workers([Node | Nodes]) -> [ init_worker(Node) | init_workers(Nodes) ].

init_worker(Node) ->
    io:fwrite("init_worker initializing ~p~n", [Node]),
    {Mod, Bin, _} = code:get_object_code(?MODULE),
    rpc:call(Node, code, purge, [Mod]),
    rpc:call(Node, erlang, load_module, [Mod, Bin]),
    spawn_link(Node, ?MODULE, start_worker, []),
    io:fwrite("init_worker initialized ~p~n", [Node]),
    Node.

start_worker() ->
    register(grep_worker, self()), % {grep_worker, Node} ! Msg
    worker().

worker() ->
    receive
        {From, Content} -> From ! match_emails(Content);
        Whatever -> io:fwrite("worker unexpectedly received ~p~n", [Whatever])
    end,
    worker().


% ripped from Erlang and OTP in Action

ensure_contact() ->
    ContactNodes = [bootstrap@dojo],
    % ContactNodes = [bootstrap@dojo, bootstrap@hal9011],
    ok = ensure_contact(ContactNodes),
    wait_for_nodes(ContactNodes, 1000).

ensure_contact(ContactNodes) ->
    Answering = [N || N <- ContactNodes, net_adm:ping(N) =:= pong],
    case Answering of
        [] -> {error, no_contact_nodes_reachable};
        _ -> wait_for_nodes(length(Answering), 1000)
    end.

wait_for_nodes(MinNodes, WaitTime) ->
    Slices = 10,
    SliceTime = round(WaitTime/Slices),
    wait_for_nodes(MinNodes, SliceTime, Slices).

wait_for_nodes(_MinNodes, _SliceTime, 0) -> ok;
wait_for_nodes(MinNodes, SliceTime, Iterations) ->
    case length(nodes()) > MinNodes of
        true -> ok;
        false ->
            timer:sleep(SliceTime),
            wait_for_nodes(MinNodes, SliceTime, Iterations - 1)
    end.


% regexp and directory walking

-spec match_emails(string()) -> [string()].
match_emails(Str) when is_list(Str) ->
    case re:run(Str, "[\\w\\._-]+@[\\w\\._-]+", [{capture, all, list}, global]) of
        {match, Emails} -> lists:append(Emails);
        nomatch -> []
    end.

scan_dir(Dir) -> filelib:fold_files(Dir, "^\\w+$", true, fun(FileName, Acc) -> [FileName | Acc] end, []).

read_file(FileName) ->
    case file:read_file(FileName) of
        {ok, Binary} -> binary_to_list(Binary);
        {error, Reason} -> throw({error, Reason})
    end.

% local search
local_grep(Dir) ->
    Files = scan_dir(Dir),
    AllEmails = lists:foldl(
        fun(FileName, Acc) ->
            Text = read_file(FileName),
            [match_emails(Text) | Acc]
        end, [], Files),
    lists:usort(lists:append(AllEmails)).


-ifdef(TEST).

-define(EMAIL1, "azx@b.com").
-define(EMAIL2, "d@efg.net").
-define(TEXT, "some text " ++ ?EMAIL1 ++ " around\n" ++ ?EMAIL2 ++ " on second line").

cluster_search_test() ->
    net_kernel:start([eunit, shortnames]),
    erlang:set_cookie(node(),dojo),
    ok = application:start(?MODULE),
    timer:sleep(1000),
    Emails = grep("/usr/share/doc/python-gi"),
    ?assert(length(Emails) > 0).

match_emails_test() ->
    Emails = match_emails(?TEXT),
    ?assert([?EMAIL1, ?EMAIL2] == Emails).

match_noemails_test() ->
    Emails = match_emails("test no emails"),
    ?assert([] == Emails).

random() -> integer_to_list(random:uniform(1000000000)).

os_tmp_dir() ->
    case os:getenv("TMPDIR") of
        false -> "/tmp";
        Dir -> Dir
    end.

tmp_dir() ->
    Name = os_tmp_dir() ++ "/" ++ random(),
    case file:make_dir(Name) of
        ok -> Name;
        {error, Reason} -> throw({error, Reason})
    end.

tmp_file(File) ->
    case file:open(File, write) of
        {ok, Dev} -> Dev;
        {error, Reason} -> throw({error, Reason})
    end.

create_file(File, Content) -> create_file(File, "~s~n", [Content]).
create_file(File, Format, Args) ->
    Dev = tmp_file(File),
    io:fwrite(Dev, Format, Args).

local_grep_test() ->
    random:seed(now()),
    Dir = tmp_dir(),
    Name1 = Dir ++ "/" ++ random(),
    Name2 = Dir ++ "/" ++ random(),
    create_file(Name1, ?TEXT),
    create_file(Name2, ?TEXT),

    FileNames = scan_dir(Dir),
    ?assert(lists:sort([Name1, Name2]) == lists:sort(FileNames)),
    ?assert(lists:sort([?EMAIL1, ?EMAIL2]) == local_grep(Dir)),

    file:delete(Name1),
    file:delete(Name2),
    file:del_dir(Dir).

-endif.
